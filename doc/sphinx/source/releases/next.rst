===========================
Changes in the next release
===========================


Summary of changes
==================

.. note:: Developers should use this page to track and list changes
          during development. At the time of release, this page should
          be published (and renamed) to list the most important
          changes in the new release.

- Add ``CellDiameter`` expression giving diameter of a cell, i.e.,
  maximal distance between any two points of the cell. Implemented
  for all simplices and quads/hexes.
- Make ``(Min|Max)(Cell|Facet)EdgeLength`` working for quads/hexes

Detailed changes
================

.. note:: At the time of release, make a verbatim copy of the
          ChangeLog here (and remove this note).
